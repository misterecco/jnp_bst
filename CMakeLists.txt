cmake_minimum_required(VERSION 3.4)
project(bst)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -Wall")

set(SOURCE_FILES_SAMPLE
        bst.h
        bst_imp.h
        my_bst_test.cpp
        )
add_executable(
        sample_bst_usage ${SOURCE_FILES_SAMPLE}
        )