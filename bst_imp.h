/*  Zadanie zaliczeniowe 7
 *  Autor: Tomasz Kępa
 *  tk359746@students.mimuw.edu.pl
 */

#ifndef BST_BST_IMP_H
#define BST_BST_IMP_H

#include "bst.h"
#include <stdexcept>
#include <algorithm>

template <typename T>
BST<T>::BST(std::initializer_list<T> values) :
        BST(values.begin(), values.end())
{ }

template <typename T>
template <typename Iter>
BST<T>::BST(Iter begin, Iter end) :
    m_root([](Iter b, Iter e) {
        NodePtr root;
        std::for_each(b, e, [&root] (T value) {
            root = (BST<T>(root) + value).m_root;
        });
        return root;
    } (begin, end))
{ }

template <typename T>
BST<T>::BST(T value, BST left, BST right) :
        m_root(std::make_shared<Node const>(Node(value, left.m_root, right.m_root)))
{ }

template <typename T>
BST<T> BST<T>::left() const {
    if (empty()) {
        throw std::logic_error("empty tree");
    }
    return BST(m_root->m_left);
}

template <typename T>
BST<T> BST<T>::right() const {
    if (empty()) {
        throw std::logic_error("empty tree");
    }
    return BST(m_root->m_right);
}

template <typename T>
T const & BST<T>::value() const {
    if (empty()) {
        throw std::logic_error("empty tree");
    }
    return m_root->m_value;
}

template <typename T>
bool BST<T>::empty() const {
    return !m_root;
}

template <typename T>
T const & BST<T>::min() const {
    if (empty()) {
        throw std::logic_error("empty tree");
    }
    return left().empty() ? value() : left().min();
};

template <typename T>
T const & BST<T>::max() const {
    if (empty()) {
        throw std::logic_error("empty tree");
    }
    return right().empty() ? value() : right().max();
};

template <typename T>
template <typename Acc, typename Functor>
Acc BST<T>::fold(Acc a, Functor f) const {
    return empty() ? a : right().fold(f(left().fold(a, f), value()), f);
};

template <typename T>
BST<T> BST<T>::find(T const & t) const {
    if (empty()) {
        throw std::logic_error("tree doesn't contain such element");
    }
    return t < value() ? left().find(t) : (t > value() ? right().find(t) : *this);
}

template <typename T>
std::size_t BST<T>::size() const {
    return empty() ? 0 : 1 + right().size() + left().size();
}

template <typename T>
std::size_t BST<T>::height() const {
    return empty() ? 0 : 1 + std::max(right().size(), left().size());
}

template <typename T>
BST<T> spine(BST<T> tree) {
    return tree.fold(BST<T>(), [](BST<T> tree, T value) {
        return BST<T>(value, tree, BST<T>());
    });
}

template <typename T>
BST<T> operator+(T value, BST<T> tree) {
    if (tree.empty()) {
        return BST<T>(value, BST<T>(), BST<T>());
    }
    return value <= tree.value() ?
       BST<T>(tree.value(), value + BST<T>(tree.left()), BST<T>(tree.right())) :
       BST<T>(tree.value(), BST<T>(tree.left()), value + BST<T>(tree.right()));
}

template <typename T>
BST<T> operator+(BST<T> tree, T value) {
    return value + tree;
}

template <typename T>
std::ostream & operator<<(std::ostream & ostr, BST<T> tree) {
    if (tree.empty()) {
        return ostr;
    }
    tree.fold(0, [&ostr] (int, T value) {ostr << value << " "; return 0;});
    return ostr;
}

template <typename T>
T max_diff(BST<T> tree) {
    if (tree.size() < 2) {
        throw std::logic_error("tree has less than 2 elements");
    }
    std::pair<T, T> acc = {T(), tree.min()}; // best result, previous node value
    acc = tree.fold(acc, [] (std::pair<T, T> acc, T value) {
        return std::pair<T, T>(std::max(acc.first, value - acc.second), value);
    });
    return acc.first;
}

#endif //BST_BST_IMP_H
